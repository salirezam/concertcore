﻿using ConcertCore.Data;
using ConcertCore.DTOs;
using ConcertCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;

namespace ConcertCore.Controllers.Api
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class AttendancesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public AttendancesController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public ActionResult Attend([FromBody] AttendanceDTO dto)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (_context.Attendances.Any(a => a.AttendeeId == userId && a.ConcertId == dto.ConcertId))
                return BadRequest("The attendance already exist.");

            var attendance = new Attendance
            {
                ConcertId = dto.ConcertId,
                AttendeeId = userId
            };

            _context.Attendances.Add(attendance);
            _context.SaveChanges();

            return Ok("OK");
        }
    }
}