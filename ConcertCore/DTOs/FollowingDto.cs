﻿namespace ConcertCore.DTOs
{
    public class FollowingDto
    {
        public string FolloweeId { get; set; }
    }
}