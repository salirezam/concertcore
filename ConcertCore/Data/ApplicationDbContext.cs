﻿using ConcertCore.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ConcertCore.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Concert> Concerts { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<Following> Followings { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<UserNotification> UserNotifications { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Attendance>()
                .HasKey(a => new { a.AttendeeId, a.ConcertId });

            builder.Entity<Attendance>()
                .HasOne(a => a.Attendee)
                .WithMany(u => u.Attendances)
                .HasForeignKey(a => a.AttendeeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Attendance>()
                .HasOne(a => a.Concert)
                .WithMany(c => c.Attendances)
                .HasForeignKey(a => a.ConcertId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Following>()
                .HasKey(a => new { a.FolloweeId, a.FollowerId });

            builder.Entity<Following>()
                .HasOne(a => a.Followee)
                .WithMany(u => u.Followers)
                .HasForeignKey(a => a.FolloweeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Following>()
                .HasOne(a => a.Follower)
                .WithMany(c => c.Followees)
                .HasForeignKey(a => a.FollowerId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<UserNotification>()
                .HasKey(a => new { a.NotificationId, a.UserId });

            builder.Entity<UserNotification>()
                .HasOne(n => n.User)
                .WithMany(u => u.UserNotifications)
                .HasForeignKey(a => a.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<UserNotification>()
                .HasOne(n => n.Notification)
                .WithMany(u => u.UserNotifications)
                .HasForeignKey(n => n.NotificationId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("DefaultConnection");
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        public ApplicationDbContext()
        {
        }
    }
}
