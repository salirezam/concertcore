﻿using ConcertCore.Models;
using System.Collections.Generic;

namespace ConcertCore.ViewModels
{
    public class ConcertViewModel
    {
        public IEnumerable<Concert> UpcomingConcerts { get; set; }
        public bool ShowActions { get; set; }
        public string Heading { get; set; }
        public string SearchTerm { get; set; }
    }
}