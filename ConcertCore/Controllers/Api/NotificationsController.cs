﻿using AutoMapper;
using ConcertCore.Data;
using ConcertCore.DTOs;
using ConcertCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace ConcertCore.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NotificationsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        public NotificationsController(IMapper mapper)
        {
            _context = new ApplicationDbContext();
            _mapper = mapper;
        }
        public IEnumerable<NotificationDto> GetNewNotifications()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var notifications = _context.UserNotifications
                .Where(un => un.UserId == userId && !un.IsRead)
                .Select(un => un.Notification)
                .Include(n => n.Concert.Artist)
                .ToList();

            return notifications.Select(_mapper.Map<Notification, NotificationDto>);
        }

        [HttpPost("markAsRead")]
        public ActionResult MarkAsRead()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var notifications = _context.UserNotifications
                .Where(un => un.UserId == userId && !un.IsRead)
                .ToList();

            notifications.ForEach(n => n.Read());

            _context.SaveChanges();

            return Ok();
        }
    }
}