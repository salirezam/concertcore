﻿namespace ConcertCore.Models
{
    public enum NotificationType
    {
        ConcertCanceled = 1,
        ConcertUpdated = 2,
        ConcertCreated = 3
    }
}
