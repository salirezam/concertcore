﻿using ConcertCore.Data;
using ConcertCore.DTOs;
using ConcertCore.Models;
using ConcertCore.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;

namespace ConcertCore.Controllers.Api
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class FollowingsController : Controller
    {
        private readonly ApplicationDbContext _context;
        public FollowingsController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IActionResult Follow([FromBody] FollowingDto dto)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (_context.Followings.Any(f => f.FolloweeId == userId && f.FolloweeId == dto.FolloweeId))
                return BadRequest("Following already exists.");

            var following = new Following
            {
                FollowerId = userId,
                FolloweeId = dto.FolloweeId
            };
            _context.Followings.Add(following);
            _context.SaveChanges();

            return Ok();
        }

        [Authorize]
        public IActionResult Following()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var followers = _context.Followings
                .Where(c => c.FolloweeId == userId)
                .Select(c => c.Follower)
                .ToList();

            var viewModel = new FollowerViewModel
            {
                Followers = followers
            };

            return View(viewModel);
        }
    }
}