﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace ConcertCore.Models
{
    public class Notification
    {
        public int Id { get; private set; }
        public DateTime DateTime { get; private set; }
        public NotificationType Type { get; private set; }
        public DateTime? OriginalDateTime { get; private set; }
        public string OriginalVenue { get; private set; }

        [Required]
        public Concert Concert { get; private set; }

        public ICollection<UserNotification> UserNotifications { get; set; }

        protected Notification()
        {
            UserNotifications = new Collection<UserNotification>();
        }

        private Notification(Concert concert, NotificationType type)
        {
            DateTime = DateTime.Now;
            Concert = concert ?? throw new ArgumentNullException("concert");
            Type = type;
        }

        public static Notification ConcertCreated(Concert concert)
        {
            return new Notification(concert, NotificationType.ConcertCreated);
        }

        public static Notification ConcertCanceled(Concert concert)
        {
            return new Notification(concert, NotificationType.ConcertCanceled);
        }

        public static Notification ConcertUpdated(Concert newConcert, DateTime originalDateTime, string originalVenue)
        {
            var notification = new Notification(newConcert, NotificationType.ConcertUpdated);
            notification.OriginalDateTime = originalDateTime;
            notification.OriginalVenue = originalVenue;

            return notification;
        }

    }
}
