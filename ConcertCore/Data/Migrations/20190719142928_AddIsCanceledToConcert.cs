﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConcertCore.Data.Migrations
{
    public partial class AddIsCanceledToConcert : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCanceled",
                table: "Concerts",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCanceled",
                table: "Concerts");
        }
    }
}
