﻿using ConcertCore.Models;
using System.Collections.Generic;

namespace ConcertCore.ViewModels
{
    public class FollowerViewModel
    {
        public IEnumerable<ApplicationUser> Followers { get; set; }
    }
}
