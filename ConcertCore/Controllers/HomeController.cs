﻿using ConcertCore.Data;
using ConcertCore.Models;
using ConcertCore.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics;
using System.Linq;

namespace ConcertCore.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController()
        {
            _context = new ApplicationDbContext();
        }
        public IActionResult Index(string query = null)
        {
            var upcomingConcerts = _context.Concerts
                .Include(c => c.Artist)
                .Include(c => c.Genre)
                .Where(c => c.DateTime > DateTime.Now && !c.IsCanceled);

            if (!string.IsNullOrWhiteSpace(query))
            {
                upcomingConcerts = upcomingConcerts.Where(c =>
               c.Artist.FirstName.Contains(query) ||
               c.Artist.LastName.Contains(query) ||
               c.Genre.Name.Contains(query) ||
               c.Venue.Contains(query));
            }
            var viewModel = new ConcertViewModel
            {
                UpcomingConcerts = upcomingConcerts,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "Upcoming Concerts",
                SearchTerm = query
            };

            return View("_Concerts", viewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
