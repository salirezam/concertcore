﻿using ConcertCore.Data;
using ConcertCore.Models;
using ConcertCore.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;

namespace ConcertCore.Controllers
{
    public class ConcertController : Controller
    {
        private readonly ApplicationDbContext _context;
        public ConcertController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IActionResult Search(ConcertViewModel viewModel)
        {
            return RedirectToAction("Index", "Home", new { query = viewModel.SearchTerm });
        }

        [Authorize]
        public IActionResult Create()
        {
            var viewModel = new ConcertFormViewModel
            {
                Genres = _context.Genres.ToList(),
                Heading = "Add a concert"
            };

            return View("Form", viewModel);
        }

        [Authorize]
        public IActionResult Edit(int id)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var concert = _context.Concerts.Single(c => c.Id == id && c.ArtistId == userId);

            var viewModel = new ConcertFormViewModel
            {
                Id = concert.Id,
                Venue = concert.Venue,
                Genre = concert.GenreId,
                Date = concert.DateTime.ToString("d MMM yyyy"),
                Time = concert.DateTime.ToString("HH:mm"),
                Genres = _context.Genres.ToList(),
                Heading = "Edit a concert"
            };

            return View("Form", viewModel);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ConcertFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.Genres = _context.Genres.ToList();
                return View("Form", viewModel);
            }

            var concert = new Concert
            {
                ArtistId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                DateTime = viewModel.GetDateTime(),
                GenreId = viewModel.Genre,
                Venue = viewModel.Venue
            };

            _context.Concerts.Add(concert);
            _context.SaveChanges();

            return RedirectToAction("Mine", "Concert");
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ConcertFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.Genres = _context.Genres.ToList();
                return View("Form", viewModel);
            }


            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var concert = _context.Concerts
                .Include(c => c.Attendances)
                .ThenInclude(a => a.Attendee)
                .Single(c => c.Id == viewModel.Id && c.ArtistId == userId);

            concert.Modify(viewModel.GetDateTime(), viewModel.Venue, viewModel.Genre);

            _context.SaveChanges();

            return RedirectToAction("Mine", "Concert");
        }

        [Authorize]
        public IActionResult Attending()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var concerts = _context.Attendances
                .Where(c => c.AttendeeId == userId)
                .Select(c => c.Concert)
                .Include(c => c.Artist)
                .Include(c => c.Genre)
                .ToList();

            var viewModel = new ConcertViewModel
            {
                UpcomingConcerts = concerts,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "Concerts I'm attending"
            };

            return View("_Concerts", viewModel);
        }

        [Authorize]
        public IActionResult Mine()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var concerts = _context.Concerts
                .Where(c => c.ArtistId == userId &&
                c.DateTime > DateTime.Now &&
                !c.IsCanceled)
                .Include(c => c.Genre)
                .ToList();

            return View(concerts);
        }
    }
}
