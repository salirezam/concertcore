﻿using ConcertCore.Attributes;
using ConcertCore.Controllers;
using ConcertCore.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace ConcertCore.ViewModels
{
    public class ConcertFormViewModel
    {
        [Required]
        public string Venue { get; set; }

        [Required]
        [FutureDate]
        public string Date { get; set; }

        [Required]
        [ValidTime]
        public string Time { get; set; }

        [Required]
        public byte Genre { get; set; }
        public IEnumerable<Genre> Genres { get; set; }
        public int Id { get; set; }
        public string Heading { get; set; }

        public string Action
        {
            get
            {
                Expression<Func<ConcertController, IActionResult>> update = (c => c.Edit(this));
                Expression<Func<ConcertController, IActionResult>> create = (c => c.Create(this));

                var action = (Id != 0) ? update : create;
                return (action.Body as MethodCallExpression).Method.Name;
            }
        }

        public DateTime GetDateTime()
        {
            return DateTime.Parse(string.Format("{0} {1}", Date, Time));
        }
    }
}
