﻿using AutoMapper;
using ConcertCore.DTOs;
using ConcertCore.Models;

namespace ConcertCore.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationUser, UserDto>();
            CreateMap<Concert, ConcertDto>();
            CreateMap<Notification, NotificationDto>();
        }
    }
}