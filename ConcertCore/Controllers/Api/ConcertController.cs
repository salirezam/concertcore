﻿using ConcertCore.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;

namespace ConcertCore.Controllers.Api
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ConcertController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public ConcertController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpDelete("{id}")]
        public ActionResult Cancel(int id)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var concert = _context.Concerts
                .Include(c => c.Attendances)
                .ThenInclude(a => a.Attendee)
                .Single(c => c.Id == id && c.ArtistId == userId);

            if (concert.IsCanceled)
                return NotFound();

            concert.Cancel();
            _context.SaveChanges();

            return Ok();
        }
    }
}